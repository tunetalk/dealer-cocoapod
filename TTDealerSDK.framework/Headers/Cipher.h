//
//  Cipher.h
//  AES
//
//  Created by Voon Sze Ching on 11/11/12.
//  Copyright (c) 2012 vsching. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <CommonCrypto/CommonDigest.h>
#import <CommonCrypto/CommonCryptor.h>

@interface Cipher : NSObject {
    NSString* cipherKey;
}

@property (retain) NSString* cipherKey;

- (Cipher *) initWithKey:(NSString *) key;
- (NSData *) encrypt:(NSData *) plainText;
- (NSData *) decrypt:(NSData *) cipherText;
- (NSData *) transform:(CCOperation) encryptOrDecrypt data:(NSData *) inputData;
+ (NSData *) md5:(NSString *) stringToHash;

- (NSString*) generateRandomString;
- (NSData *) convertToJsonData:(NSMutableDictionary*) dic;
- (id *) convertToJsonObject:(NSData*) data;

@end